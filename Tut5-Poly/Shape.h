#pragma once
#ifndef Shape_H
#define Shape_H
#include<string>
class Shape
{
private:
	const string name;
	double radius, base, height, width, breadth;
public:
	Shape(const string &, double radius, double base, double height, double width, double breadth);                             //constructor
	~Shape();                            //destructor
	 
	//getting and setting parameters
		void setName(const string &);
		void setRadius(double );
		void setBase(double );
		void setHeight(double );
		void setWidth(double);
		void setBreadth(double);
	//function definitions

	virtual void name();
	virtual double perimeter();
	virtual double area();
	virtual void draw();
};
//concrete classes
class Circle
{
private:
	double radius;
public: Circle();                             
	   ~Circle();
	   void setRadius(double radius);
	   double getRadius();

	   virtual void name();
	   virtual double perimeter(double);
	   virtual double area(double);
	   virtual void draw();
};
class Triangle
{
private:
	double base, height;
public:
	Triangle();
	~Triangle();
	void setBase(double base);
	void setHeight(double height);
	double getBase();
	double getHeight();

	virtual void name();
	virtual double perimeter(double,double);
	virtual double area(double,double);
	virtual void draw();
};
class Rectangle
{
private:
	double width, breadth;
public:
	Rectangle();
	~Rectangle();
	void setWidth(double width);
	void setBreadth(double breadth);
	double getwidth();
	double getBredth();

	virtual void name();
	virtual double perimeter(double,double);
	virtual double area(double,double);
	virtual void draw();
};
#endif